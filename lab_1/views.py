from django.shortcuts import render
from datetime import date

### Enter your name here
##mhs_name = 'Azahra Putri Andani' # TODO Implement this
##birth_year = 1998
##
### Create your views here.
##def index(request):
##    response = {'name': mhs_name, 'age' : calculate_age(birth_year)}
##    return render(request, 'index.html', response)
##
### TODO Implement this to complete last checklist
##def calculate_age(birth_year):
##    return date.today().year - birth_year
##    pass

from datetime import datetime, date
# Enter your name here
mhs_name = 'Azahra Putri' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,3,14) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

