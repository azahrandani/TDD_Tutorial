# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import *
# from .models import Friend


# class Lab7UnitTest(TestCase):
# 	def test_lab_7_url_is_exist(self):
# 		response = Client().get('/lab-7/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_lab7_using_index_func(self):
# 		found = resolve('/lab-7/')
# 		self.assertEqual(found.func, index)

# 	def test_model_can_create_new_friend(self):
#             # Creating a new activity
# 		new_friend = Friend.objects.create(friend_name='Azahra', npm='1606827851')

#             # Retrieving all available activity
# 		counting_all_available_friend = Friend.objects.all().count()
# 		self.assertEqual(counting_all_available_friend, 1)

# 	def test_lab7_post_success(self):
# 		nama = 'Azahra'
# 		npm = '1606827851'
# 		alamat = 'Bogor'
# 		ttl = 'Bogor, 14 Maret 1998'
# 		prodi = 'Ilkom'
# 		kamus = {'name': nama, 'npm': npm, 'alamat': alamat, 'ttl': ttl, 'prodi': prodi}
# 		response_post = Client().post('/lab-7/add-friend/', kamus)
# 		self.assertEqual(response_post.status_code, 200)
# 		self.assertEqual(dict, type(response))


# 	def test_daftar_teman_url_is_exist(self):
# 		response = Client().get('/lab-7/get-friend-list/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_lab7_using_get_friend_list_func(self):
# 		found = resolve('/lab-7/get-friend-list/')
# 		self.assertEqual(found.func, get_friend_list)

# 	def test_new_friend_in_friendList(self):
# 		new_friend = Friend.objects.create(friend_name='Azahra', npm='1606827851')
# 		response = Client().get('/lab-7/get-friend-list/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_npm_valid(self):
# 		new_friend = Friend.objects.create(friend_name='Azahra', npm='1606827851')
# 		self.assertTrue(npmValid('1606812345'))
# 		self.assertFalse(npmValid('1606827851'))

# 	def test_validate_npm(self):
# 		new_friend = Friend.objects.create(friend_name='Azahra', npm='1606827851')
# 		response = Client().post('/lab-7/validate-npm/', {'npm':'1606827851'})
# 		self.assertEqual(dict, type(response.json()))