// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '142524809841170',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11',
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  function facebookLogin(){
    FB.login(function(response) {
    if (response.authResponse) {
     console.log('Welcome!  Fetching your information.... ');
     FB.api('/me', function(response) {
       console.log('Good to see you, ' + response.name + '.');
     });

    // var lb = document.getElementById("login_button");
    // if (lb.style.display === "block") {
    //     lb.style.display = "none";
    // }

    $('#login_button').hide();

    var login_aval = document.getElementById("login_available");
    if (login_aval.style.display === "none") {
        login_aval.style.display = "block";
    }

    

    } else {
     console.log('User cancelled login or did not fully authorize.');
    }
  });
  };

//   const facebookLogin = () => {
//     // TODO: Implement Method Ini
//     // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
//     // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
//     // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
//     FB.login(function(response) {
//     if (response.authResponse) {
//      console.log('Welcome!  Fetching your information.... ');
//      FB.api('/me', function(response) {
//        console.log('Good to see you, ' + response.name + '.');
//      });
//     } else {
//      console.log('User cancelled login or did not fully authorize.');
//     }
// });
//   };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected') {
            FB.logout(function(response) {
                document.location.reload();
            });
        }
    });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = () => {
    var x = document.getElementById("profile_data");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    
    FB.getLoginStatus(function(response){
      if(response.status === 'connected'){
    FB.api('/me?fields=name,first_name,last_name, picture.width(200).height(200), age_range, id, gender, email', 'GET', function (response){
      console.log(response);
      document.getElementById('firstname').innerHTML = response.first_name;
      document.getElementById('lastname').innerHTML = response.last_name;
      document.getElementById('name').innerHTML = response.name;
      document.getElementById('gender').innerHTML = response.gender;
      document.getElementById('age').innerHTML = JSON.stringify(response.age_range['min']);
      
    });
      }
      else{
        console.log('not login apa kek');
      }
      
    });
  
  };

  const getUserFeed = () => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut

        // var pageID = "Me",
        //     accessToken = "EAACBoCSpihIBADJZAxovYZAXXY0ipwL3VtCqcraDFi8ZCzNtyTxOlgpMNvZA3Egw1HeTgXgNvQXv3f74Wp8rTPeQNMk8IYxzNyDjKxUTbflBgITANOzQhceqNvJQe8AE12K3WkYr11T0P60MVZCggktLgMrY7TYZA1rVToZAulNzqCqWCfJI2jsvMtLBkGAgDLDrIhPAJmqsQZDZD";
        // var postsURL = "https://graph.facebook.com/" + pageID + "/feed?access_token=" + accessToken;

        // $.ajax({
        //     url: postsURL,
        //     method: 'GET',
        //     dataType: "jsonp",
        //     success: function (data)
        //     {
        //         console.log("Successfully retrieved Facebook data");
        //         console.dir(data);
        //     },
        //     error: function(status) {
        //         console.log("Facebook data could not be retrieved.  Failed with a status of " + status);
        //     }
        // });

//     FB.api(
//     "/me/feed",
//     function (response) {
//       if (response && !response.error) {
//         console.log('sukses status')
//         console.log(response.name)
//       }
//     }
// );

FB.api(
    "/{page-id}/feed?limit=1",
    function (response) {
      if (response && !response.error) {
        /* handle the result */
      }
    }
);
  };

  // const postFeed = () => {
  //   // Todo: Implement method ini,
  //   // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  //   // Melalui API Facebook dengan message yang diterima dari parameter.

  // };

  const postStatus = () => {
    // const message = $('#postInput').val();
    // postFeed(message);

    FB.ui({
    method: 'share',
    href: 'zahra-first-try.herokuapp.com/lab-8',
      }, function(response){});
  };
