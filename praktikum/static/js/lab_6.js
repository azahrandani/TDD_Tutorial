$(document).ready(function(){

var counter = 0;

$(".chat-text").bind("keyup", function(send){
	// console.log(send.keyCode);
	
	var str = $("textarea").val();
	if(send.keyCode == 13){
		
		if(counter%2==0){
			$(".msg-insert").append("<div class = \"msg-send\">" + str + "</div>");
		}
		else{
			$(".msg-insert").append("<div class = \"msg-receive\">" + str + "</div>");
		}
		
		counter++;
		$("textarea").val('');
	}
});


});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac' || erase = true) {
    print.value='';
    erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value));
      erase = true;
  }
  else if(x === 'log'){
  	print.value = Math.log10(evil(print.value));
  }
  else if(x === 'sin'){
  	print.value = Math.sin(evil(print.value));
  }
  else if(x === 'tan'){
  	print.value = Math.tan(evil(print.value));
  }
   else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}

function changeTheme(x){
	$("body").css({"backgroundColor" : x["bcgColor"]});
	$("h1").css({"color" : x["fontColor"]});
}

if(localStorage.getItem("themes") === null){
	localStorage.setItem("themes", JSON.stringify(themes));}

if(localStorage.getItem("selectedTheme") === null){
	localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
}
themes = JSON.parse(localStorage.getItem("themes"));
selectedTheme = JSON.parse(localStorage.getItem("selectedTheme"));

changeTheme(selectedTheme);

$(document).ready(function() {
    $('.my-select').select2({'data': themes}).val(selectedTheme['id']).change();
    $('.apply-button').on('click', function(){
    	selectedTheme = themes[$('.my-select').val()];
    	changeTheme(selectedTheme);
    	localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
    })
});